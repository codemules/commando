﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	The contents of this file are Proprietary and confidential. Unauthorized copying 
//	of this file, via any medium is strictly prohibited.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using System ;
using System.Collections.Generic ;
using System.Reflection ;
using System.Linq ;
using System.Text.RegularExpressions ;

namespace CodeMules.Commando {
	public class CommandoCommands : ICommandoCommands {
		private EventHandler<ItemChangedEventArgs<CommandoCommandAttribute>> changedListeners ;
		private IDictionary<string,Func<ICommandoLogWriter,string [],object>> commands ;
		private IDictionary<string,CommandoCommandAttribute> commandInfo ;

		public CommandoCommands() {
			commandInfo = new Dictionary<string, CommandoCommandAttribute>(StringComparer.CurrentCultureIgnoreCase) ;
			commands = new Dictionary<string, Func<ICommandoLogWriter, string[], object>>(StringComparer.CurrentCultureIgnoreCase) ;
		}

		#region ICommandoCommands implementation

		public event EventHandler<ItemChangedEventArgs<CommandoCommandAttribute>> Changed {
			add {
				changedListeners += value ;
				if (commandInfo.Count > 0) {
					value(this,new ItemChangedEventArgs<CommandoCommandAttribute>(ItemChangeType.Add,null)) ;
				}
			}

			remove {
				changedListeners -= value ;
			}
		}

		public void AddCommandsForType(
						Type type, 
						Action<
							CommandoCommandAttribute, 
							MethodInfo, 
							LogWriterStyle,
							string
							> callback = null
						) 
		{
			AddCommands(null,type,callback) ;
		}

		public void AddCommands(
			object commandInstance, 
			Type type = null, 
			Action<
				CommandoCommandAttribute, 
				MethodInfo, 
				LogWriterStyle,
				string
				> callback = null
			)
		{
			BindingFlags bindingFlags = BindingFlags.Static ;

			if (commandInstance != null) {
				type = commandInstance.GetType();
				bindingFlags |= BindingFlags.Instance ;
			}

			IEnumerable<MethodInfo> commandMethods = GetCommandoMethods(
				type,
				bindingFlags
			) ;

			foreach(MethodInfo method in commandMethods) {
				CommandoCommandAttribute attr = GetCommandoCommandAttribute(method) ;

				// @@SANITY
				if (attr == null)
					continue ;

				if (commandInfo.ContainsKey(attr.FullName)) {
					if (callback != null) {
						callback(
							attr,
							method,
							LogWriterStyle.Warning,
							string.Format(
								"A command with the name '{0}' (from {1}.{2}) already exists",
								attr.FullName,
								method.DeclaringType.FullName,
								method.Name
							));
					}

					continue ;
				}

				Func<ICommandoLogWriter,string [],object> commandFunc =
					GetCommandFunction(method,commandInstance) ;

				if (commandFunc == null) {
					if (callback != null) {
						callback(
							attr,
							method,
							LogWriterStyle.Error,
							string.Format(
								"Unable to map {0}.{1} to supported Commando command signature",
								method.DeclaringType.FullName,
								method.Name
							));
					}

					continue ;
				}

				AddCommand(attr) ;

				if (!commands.ContainsKey(attr.Name)) {
					commands.Add(attr.Name,commandFunc) ;

					if (callback != null) {
						callback(
							attr,
							method,
							LogWriterStyle.Success,
							string.Format(
								"Command registered successfully '{0}'",
								attr.Name
							));
					}
				} else {
					if (callback != null) {
						callback(
							attr,
							method,
							LogWriterStyle.Warning,
							string.Format(
								"Unable to register command with its simple name '{0}' as it already exists.",
								attr.Name
							));
					}
				}

				if ((attr.Alias != null) && !commands.ContainsKey(attr.Alias)) {
					commands.Add(attr.Alias,commandFunc) ;
				}

				if (attr.Namespace != null) {
					if (!commands.ContainsKey(attr.FullName)) {
						commands.Add(attr.FullName,commandFunc) ;

						if (callback != null) {
							callback(
								attr,
								method,
								LogWriterStyle.Success,
								string.Format(
									"Command registered with fully qualified name '{0}'",
									attr.FullName
								));
						}
					} else {
						if (callback != null) {
							callback(
								attr,
								method,
								LogWriterStyle.Warning,
								string.Format(
									"Unable to register command with its fully qualified name '{0}' as it already exists.",
									attr.FullName
								));
						}
					}

					if ((attr.Alias != null) && !commands.ContainsKey(attr.Alias)) {
						commands.Add(attr.FullNameAlias,commandFunc) ;
						
						if (callback != null) {
							callback(
								attr,
								method,
								LogWriterStyle.Success,
								string.Format(
									"Command registered with fully qualified alias '{0}'",
									attr.FullNameAlias
								));
						}
					}
				}
			}	
		}

		public IEnumerable<CommandoCommandAttribute> Select(Predicate<CommandoCommandAttribute> selector) {
			return from p in Commands where ((selector == null) || selector(p)) select p ;
		}

		public bool HasCommand (string commandFullName) {
			return commands.ContainsKey(commandFullName) ;
		}

		public Func<ICommandoLogWriter, string[], object> GetCommandFunction (string command) {
			Func<ICommandoLogWriter,string [],object> commandFunc ;
			commands.TryGetValue(command,out commandFunc) ;
			return commandFunc ;
		}

		public IEnumerable<CommandoCommandAttribute> Commands {
			get {
				return commandInfo.Values ;
			}
		}
		
		public IEnumerable<CommandoCommandAttribute> GetCommands(string name) {
			string pattern = string.Format("^({0})+",name) ;

			return Select(
				(attr) =>
					(Regex.IsMatch(attr.FullName, pattern, RegexOptions.IgnoreCase | RegexOptions.CultureInvariant) ||
					Regex.IsMatch(attr.Name, pattern, RegexOptions.IgnoreCase | RegexOptions.CultureInvariant))
				);
		}
		
		public CommandoCommandAttribute GetCommand(string name) {
			return GetCommands(name).FirstOrDefault();
		}
		#endregion

		#region Add Commands (and related) Implementation
		private void AddCommand(CommandoCommandAttribute attr) {
			commandInfo.Add(attr.FullName,attr) ;

			if (changedListeners != null) {
				changedListeners(
					this,
					new ItemChangedEventArgs<CommandoCommandAttribute>(
						ItemChangeType.Add,
						attr
					)
				) ;
			}
		}
			
		private IEnumerable<MethodInfo> GetCommandoMethods(
			Type type,
			BindingFlags extraBindingFlags = BindingFlags.Default
		) 
		{
			return from p in type.GetMethods(BindingFlags.NonPublic|BindingFlags.Public|extraBindingFlags)
					where p.IsDefined(typeof(CommandoCommandAttribute),false)
				select p ;
		}

		private CommandoCommandAttribute GetCommandoCommandAttribute(MethodInfo method) {
			CommandoCommandAttribute result = null ;

			if (method.IsDefined(typeof(CommandoCommandAttribute),false)) {
				CommandoCommandAttribute [] attrs = method.GetCustomAttributes(typeof(CommandoCommandAttribute),false)
					as CommandoCommandAttribute [] ;

				if ((attrs != null) || (attrs.Length > 0)) 
					result = attrs[0] ;
			}

			return result ;
		}

		enum CommandSignatureType {
			None,
			NoParams,
			ArgsOnly,
			LogOnly,
			Full
		}

		private CommandSignatureType GetSignatureType(MethodInfo method) {
			CommandSignatureType result = CommandSignatureType.None ;

			ParameterInfo [] paramInfo = method.GetParameters() ;

			if (paramInfo.Length == 0) {
				result = CommandSignatureType.NoParams ;
			} else if (paramInfo.Length == 1) {
				if (paramInfo[0].ParameterType == typeof(string [])) {
					result = CommandSignatureType.ArgsOnly;
				} else if (paramInfo[0].ParameterType == typeof(ICommandoLogWriter)) {
					result = CommandSignatureType.LogOnly ;
				}
			} else if (paramInfo.Length == 2) {
				if ((paramInfo[0].ParameterType == typeof(ICommandoLogWriter)) &&
					(paramInfo[1].ParameterType == typeof(string [])))
				{
					result = CommandSignatureType.Full ;
				}
			}

			return result ;
		}

		private Func<ICommandoLogWriter,string [],object> GetCommandFunction(
			MethodInfo method,
			object target
			) 
		{
			Func<ICommandoLogWriter,string [],object> commandFunc = null ;

			CommandSignatureType sigType = GetSignatureType(method) ;

			if (sigType == CommandSignatureType.None)
				return null ;

			if (method.ReturnType == typeof(void)) {

				switch(sigType) {
				case CommandSignatureType.NoParams:
					Action actionVoid = Delegate.CreateDelegate(
						typeof(Action),
						method.IsStatic ? null : target,
						method,
						false
					) as Action ;

					if (actionVoid != null) {
						commandFunc = (logWriter,args) => { actionVoid(); return null ; } ;
					}
					break;

				case CommandSignatureType.Full:
					Action<ICommandoLogWriter,string []> action = Delegate.CreateDelegate(
						typeof(Action<ICommandoLogWriter,string []>),
						method.IsStatic ? null : target,
						method,
						false
					) as Action<ICommandoLogWriter,string []> ;

					if (action != null) {
						commandFunc = (logWriter,args) => { action(logWriter,args); return null ; } ;
					}
					break ;

				case CommandSignatureType.ArgsOnly:
					Action<string []> actionArgs = Delegate.CreateDelegate(
						typeof(Action<string []>),
						method.IsStatic ? null : target,
						method,
						false
					) as Action<string []> ;

					if (actionArgs != null) {
						commandFunc = (logWriter,args) => { actionArgs(args); return null ; } ;
					}
					break ;

				case CommandSignatureType.LogOnly:
					Action<ICommandoLogWriter> actionLog = Delegate.CreateDelegate(
						typeof(Action<ICommandoLogWriter>),
						method.IsStatic ? null : target,
						method,
						false
					) as Action<ICommandoLogWriter> ;

					if (actionLog != null) {
						commandFunc = (logWriter,args) => { actionLog(logWriter); return null ; } ;
					}
					break ;

				}

			} else {
				switch(sigType) {
				case CommandSignatureType.Full:
					commandFunc = Delegate.CreateDelegate(
						typeof(Func<ICommandoLogWriter,string [],object>),
						method.IsStatic ? null : target,
						method,
						true
					) as Func<ICommandoLogWriter,string [],object> ;
					break ;

				case CommandSignatureType.ArgsOnly:
					Func<string [],object> argsFunc = Delegate.CreateDelegate(
						typeof(Func<string [],object>),
						method.IsStatic ? null : target,
						method,
						true
					) as Func<string [],object> ;

					commandFunc = (logwriter,args) => argsFunc(args) ;
					break;

				case CommandSignatureType.LogOnly:
					Func<ICommandoLogWriter,object> logFunc = Delegate.CreateDelegate(
						typeof(Func<ICommandoLogWriter,object>),
						method.IsStatic ? null : target,
						method,
						true
					) as Func<ICommandoLogWriter,object> ;

					commandFunc = (logwriter,args) => logFunc(logwriter) ;
					break;

				case CommandSignatureType.NoParams:
					Func<object> noParamsFunc = Delegate.CreateDelegate(
						typeof(Func<object>),
						method.IsStatic ? null : target,
						method,
						true
					) as Func<object> ;

					commandFunc = (logwriter,args) => noParamsFunc() ;
					break;				
				}
			}

			return commandFunc ;
		}
		#endregion
	}
}
