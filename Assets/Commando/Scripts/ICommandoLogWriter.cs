//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	The contents of this file are Proprietary and confidential. Unauthorized copying 
//	of this file, via any medium is strictly prohibited.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using System ;

namespace CodeMules.Commando {
	public enum LogWriterStyle {
		Normal,
		Error,
		Warning,
		Success
	}

	public interface ICommandoLogWriter {
		void NextIndent() ;
		void PrevIndent();

		void WriteLine() ;

		void WriteLine(LogWriterStyle style,string logFormat,params object [] args) ;

		void WriteLine(LogWriterStyle style,string logText) ;

		void WriteLine(string logFormat,params object [] args) ;
		
		void WriteLine(string logText) ;

		void WriteLine(Exception ex) ;

		string FormatWithStyle(LogWriterStyle style,string text) ;
	}
}
