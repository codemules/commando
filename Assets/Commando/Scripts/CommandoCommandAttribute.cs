﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	The contents of this file are Proprietary and confidential. Unauthorized copying 
//	of this file, via any medium is strictly prohibited.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using System ;

namespace CodeMules.Commando {
	[AttributeUsage(AttributeTargets.Method)]
	public class CommandoCommandAttribute : Attribute {
		/// <summary>
		/// Initializes a new instance of the <see cref="CodeMules.Commando.CommandoCommandAttribute"/> class.
		/// </summary>
		/// <param name="name">The name of the command</param>
		public CommandoCommandAttribute(string name) {
			Name = name ;
		}

		/// <summary>
		/// Get the name of the command
		/// </summary>
		/// <value>The name of the command</value>
		/// <remarks>
		/// The <c>Name</c> of a command needs to be unique within its namespace but can have 
		/// the same <c>Name</c> as commands in another namespace. Still, 2+ commands 
		/// with the same <c>Name</c> are ambiguous and can only be differentiated by 
		/// namespace; something that is not ideal for the users of the commands
		/// </remarks>
		/// <seealso cref="Namespace"/>
		/// <seealso cref="FullName"/>
		public string Name { get ; private set ; }

		/// <summary>
		/// The (optional) namespace for the command
		/// </summary>
		/// <value>The namespace for the command or <c>null</c></value>
		/// <remarks>
		/// The <c>Namespace</c> of a command further identifies its origins, and combined
		/// with <see cref="Name"/> creates a unique name/identifier for the command.
		/// <para>
		/// Although <c>Namespace</c> is optional, it is highly recommended to prevent
		/// collisions with other commands
		/// </para>
		/// </remarks>
		/// <seealso cref="FullName"/>
		/// <seealso cref="Name"/>
		public string Namespace { get ; set ; }

		/// <summary>
		/// Get the full name of the command
		/// </summary>
		/// <value>The full name of the command</value>
		/// <remarks>
		/// The full name of a command is: <see cref="Namespace"/>.<see cref="Name"/>
		/// <para>
		/// In the case where namespace is undefined (<c>null</c>) then <see cref="Name"/>
		/// is equivalent to <c>FullName</c>
		/// </para>
		/// <para>
		/// The full name of a command must be universally unique. Choose a unique, meaningful
		/// namespace for your commands, and avoid defining two commands with the same <see cref="Name"/>
		/// or <see cref="Alias"/>
		/// </para>
		/// </remarks>
		/// <seealso cref="Name"/>
		/// <seealso cref="Namespace"/>
		public string FullName {
			get {
				string result = Name ;

				if (!String.IsNullOrEmpty(Namespace)) {
					result = string.Format("{0}.{1}",Namespace,Name) ;
				}

				return result ;
			}
		}

		/// <summary>
		/// Get the alias command name for the command
		/// </summary>
		/// <value>The alias command name or <c>null</c></value>
		/// <remarks>
		/// An alias is optional, but when specified, should be a synonym for
		/// <see cref="Name"/>, or an abberviation of a long name.
		/// </remarks>
		public string Alias { get ; set ; }

		/// <summary>
		/// Get the full alias name for the command
		/// </summary>
		/// <value>The full alias name of the command</value>
		/// <remarks>
		/// The full alias name of a command is: <see cref="Namespace"/>.<see cref="Name"/>
		/// <para>
		/// In the case where namespace is undefined (<c>null</c>) then <see cref="Alias"/>
		/// is equivalent to <c>FullAliasName</c>
		/// </para>
		/// <para>
		/// The full name of a command (alias or otherwise) must be universally unique. Choose a unique, meaningful
		/// namespace for your commands, and avoid defining two commands with the same <see cref="Name"/> or
		/// <see cref="Alias"/>
		/// </para>
		/// </remarks>
		/// <seealso cref="Name"/>
		public string FullNameAlias {
			get {
				string result = Alias ;

				if (!String.IsNullOrEmpty(Namespace)) {
					result = string.Format("{0}.{1}",Namespace,Alias) ;
				}

				return result ;
			}
		}

		/// <summary>
		/// Get the (optional) description of the command
		/// </summary>
		/// <value>The description of the command or <c>null</c></value>
		/// <remarks>
		/// Description is intended to be a 1-line description of the commands functionality
		/// </remarks>
		public string Description { get ; set ; }

		/// <summary>
		/// Get the (optional) syntax for the command
		/// </summary>
		/// <value>The syntax of the command or <c>null</c></value>
		/// <remarks>
		/// Syntax is intended to be a 1-line description of the command-line syntax for the command
		/// </remarks>
		public string Syntax { get ; set ; }

		/// <summary>
		/// Get the help for the command as a Commando <see cref="HelpDescriptor"/> object encoded in Json format
		/// </summary>
		/// <value>The json encoded help</value>
		/// <remarks>
		/// The Commando <see cref="HelpDescriptor"/> object json encoding format can be found <c>here</c>
		/// </remarks>
		public string HelpJson { get ; set ; }

		/// <summary>
		/// Get the (optional) url for the command help page
		/// </summary>
		/// <value>The help URL or <c>null</c></value>
		public string HelpUrl { get ; set ; }
	}
}
