﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	The contents of this file are Proprietary and confidential. Unauthorized copying 
//	of this file, via any medium is strictly prohibited.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using System ;
using System.Collections.Generic ;
using System.Reflection ;

namespace CodeMules.Commando {
	public interface ICommandoCommands {
		event EventHandler<ItemChangedEventArgs<CommandoCommandAttribute>> Changed ;

		void AddCommandsForType(
				Type type,
				Action<
					CommandoCommandAttribute,
					MethodInfo,
					LogWriterStyle,
					string
					> callback = null
				) ;

		void AddCommands(
			object commandInstance,
			Type type = null,
			Action<
				CommandoCommandAttribute,
				MethodInfo,
				LogWriterStyle,
				string
				> callback = null
			) ;

		IEnumerable<CommandoCommandAttribute> Commands { get ; }

		/// <summary>
		/// Select commands based on a predicate callback
		/// </summary>
		/// <param name="selector">The predicate/selector callback</param>
		IEnumerable<CommandoCommandAttribute> Select(Predicate<CommandoCommandAttribute> selector) ;
			

		bool HasCommand(string commandFullName) ;
		
		CommandoCommandAttribute GetCommand(string name) ;
		IEnumerable<CommandoCommandAttribute> GetCommands(string name) ;
		
		Func<ICommandoLogWriter,string [],object> GetCommandFunction(string command) ;
	}
}
