
using System.Collections.Generic;

namespace CodeMules.Commando {

	public interface IHelpDescriptor {
		string Summary { get ; }

		string FullDescription { get ; }

		string Syntax { get ; }

		IEnumerable<IArgDescriptor> Args { get ; }

		IEnumerable<string> Examples { get ; }

		IEnumerable<string> SeeAlso { get ; }

		string Remarks { get ; }
	}

}
