
using System.Collections.Generic;

namespace CodeMules.Commando {

	public interface IArgDescriptor {
		string Name { get ; }

		string Description { get ; }

		bool IsRequired { get ; }

		string ValueType { get ; }

		string ValueExamples { get ; }

		IEnumerable<string> Aliases { get ; }
	}
}
