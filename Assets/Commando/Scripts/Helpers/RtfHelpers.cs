﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	The contents of this file are Proprietary and confidential. Unauthorized copying 
//	of this file, via any medium is strictly prohibited.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

namespace CodeMules.Commando {
	public static class RtfHelpers {
		/// <summary>
		/// Add <c>Bold</c> text formatting
		/// </summary>
		/// <param name="str">The string to format</param>
		public static string Bold(this string str) {
			return string.Format ("<b>{0}</b>", str);
		}
		
		/// <summary>
		/// Add <c>Italic</c> text formatting
		/// </summary>
		/// <param name="str">The string to format</param>
		public static string Italic(this string str) {
			return string.Format ("<i>{0}</i>", str);
		}
		
		/// <summary>
		/// Add font size formatting
		/// </summary>
		/// <param name="message">the string to format</param>
		/// <param name="size">The font size in pixels</param>
		public static string FontSize(this string str, int fontSize) {
			return string.Format ("<size={0}>{1}</size>", fontSize, str);
		}
		
		/// <summary>
		/// Add <c>Color</c> text formatting using custom color information
		/// </summary>
		/// <param name="str">the string to format</param>
		/// <param name="color">The color information for formatting</param>
		public static string Color(this string str, string colorInfo) {
			return string.Format("<color={0}>{1}</color>", colorInfo, str);
		}
	}
}
