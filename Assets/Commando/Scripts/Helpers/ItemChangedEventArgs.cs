//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	The contents of this file are Proprietary and confidential. Unauthorized copying 
//	of this file, via any medium is strictly prohibited.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using System ;

namespace CodeMules.Commando {
	public enum ItemChangeType {
		Add,
		Remove
	}
	public class ItemChangedEventArgs<T> : EventArgs {
		public ItemChangedEventArgs(ItemChangeType changeType,T item) {
			ChangeType = changeType ;
			Item = item ;
		}
		
		public ItemChangeType ChangeType { get ; private set ; }
		public T Item { get ; private set ; }
	}
}
