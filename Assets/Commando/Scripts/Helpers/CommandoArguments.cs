﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	The contents of this file are Proprietary and confidential. Unauthorized copying 
//	of this file, via any medium is strictly prohibited.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using System ;
using System.Collections.Generic;

namespace CodeMules.Commando {
	public static class CommandoArguments {
		#region Command Line Parsing
		public static IEnumerable<string> ParseCommandLine(string commandLine) {
			List<string> args = new List<string>() ;

			IEnumerator<string> argList = ParseArgs(commandLine) ;

			while(argList.MoveNext()) {
				args.Add(argList.Current) ;
			}

			return args ;
		}

		public static IEnumerator<string> ParseArgs(string commandLine) {
			const char ArgSeparatorChar = ' ' ;
			const char ArgDelimiterChar = '"' ;

			int startIndex = 0;
			char separator = ArgSeparatorChar;

			commandLine = commandLine.Trim();

			int i = 0;
			while(i < commandLine.Length) {
				char c = commandLine[i];
				char? next = ((i + 1) < commandLine.Length) ? commandLine[i + 1] : (char?) null;

				if (c == separator) {
					// skip it if it looks like a quoted delimiter
					if (separator == ArgDelimiterChar) {
						if (next.HasValue && (next == c)) {
							i += 2;
							continue;
						}
					}

					if (i != startIndex) {
						// return the arg, removing any quotes that were quoted
						yield return commandLine.Substring(startIndex, i - startIndex).Replace("\"\"","\"") ;

						// if arg was quoted, then skip to the arg separator, ignoring
						// anything after the final delimiter
						if (separator == ArgDelimiterChar) {
							// skip to expected ' ' (or EOL)
							while ((i < commandLine.Length) && (commandLine[i] != ArgSeparatorChar))
								i++;
						}
					} else {
						// empty arg
						yield return string.Empty ;
					}

					// Skip arg separator
					i++;

					// skip whitespace
					while ((i < commandLine.Length) && char.IsWhiteSpace(commandLine[i]))
						i++;

					separator = ArgSeparatorChar;

					startIndex = i ;

					continue;
					// check for arg that is delimited
				} else if ((i == startIndex) && (c == ArgDelimiterChar)) {
					separator = ArgDelimiterChar;
					// we don't want the quote in the result so skip past it
					startIndex++;
				}

				if (i == commandLine.Length - 1) {
					yield return commandLine.Substring(startIndex, commandLine.Length - startIndex) ;
				}

				i++;
			}

			yield break;
		}
		#endregion

		#region Argument Parsing
		/// <summary>
		///	Process command-line arguments into Key/Value pairs
		/// </summary>
		/// <returns>
		/// The arguments as key/value pairs
		/// </returns>
		/// <param name='args'>
		/// the command-line arguments
		/// </param>
		/// <remarks>
		/// Command-line parameters are required to have the form <c>-{key}[':','=']{value}</c>. 
		/// </remarks>
		public static IDictionary<string,object> ParseArguments(string [] args,Func<string,string,object> argCooker = null) {
			IDictionary<string,object> arguments = new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase) ;
			
			foreach(string arg in args) {
				if (arg.Length == 0)
					continue ;

				// if it doesnt have a prefix then it is considered unbound/free and can only
				// be referenced by its index (based on order specified)
				if (arg[0] != '-') {
					List<string> freeArgs;
					object freeArgsObj;
					if (!arguments.TryGetValue ("-free", out freeArgsObj)) {
						arguments.Add ("-free", freeArgs = new List<string> ());
					} else {
						freeArgs = freeArgsObj as List<string>;
					}

					freeArgs.Add (arg);
				} else {
					KeyValuePair<string,object> argPair = ParseArgument (arg, argCooker);
					arguments.Add (argPair.Key, argPair.Value);
				}
			}
			
			return arguments ;
		}
		
		/// <summary>
		/// Parse an argument into a Key/Value pair
		/// </summary>
		/// <returns>
		/// The argument as a <see cref="KeyValuePair[string,string]"/>
		/// </returns>
		/// <param name='arg'>
		/// The command-line argument
		/// </param>
		/// <remarks>
		/// Expected format of argument is: -{key}:{value} or,
		/// -{key}={value}
		/// <para>
		/// {value} is optional and if not specified defaults to "true"
		/// </para>
		/// </remarks>
		public static KeyValuePair<string,object> ParseArgument(string arg,Func<string,string,object> argCooker) {
			arg = arg.Trim() ;
			
			if (arg[0] != '-') {
				throw new ArgumentException("Expecting argument to have '-' prefix") ;
			}
			
			int startOfValue = arg.IndexOfAny(new [] { ':', '='}) ;
			
			string argName = (startOfValue !=-1) ? arg.Substring(1,startOfValue-1) : arg ;
			object argValue = ((startOfValue != -1) && (startOfValue+1 < arg.Length)) ? (object) arg.Substring(startOfValue+1) : true;

			if ((argCooker != null) && (argValue is string)) {
				argValue = argCooker(argName, argValue as string);
			}

			return new KeyValuePair<string, object>(argName,argValue) ;
		}
		
		/// <summary>
		/// Get the value of an option
		/// </summary>
		/// <returns>The option value</returns>
		/// <param name="options">The options collection</param>
		/// <param name="key">The option key</param>
		/// <param name="defaultValue">Default value returned if key is not defined</param>
		/// <typeparam name="T">The type of the option</typeparam>
		public static T GetOption<T>(IDictionary<string,object> options,string key,T defaultValue) {
			object result;
			
			if (!options.TryGetValue (key, out result)) {
				result = defaultValue ;
			}
			
			return (T) result;
		}

		public static string GetFreeOption(IDictionary<string,object> options,int index,string defaultValue) {
			string result = defaultValue;

			List<string> freeArgs;
			object freeArgsObj;

			if (options.TryGetValue("-free", out freeArgsObj)) {
				freeArgs = freeArgsObj as List<string>;
				result = freeArgs[index];
			}

			return result;
		}
		#endregion
	}
}
