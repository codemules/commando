﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	The contents of this file are Proprietary and confidential. Unauthorized copying 
//	of this file, via any medium is strictly prohibited.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using System ;
using System.Collections.Generic;
using System.Linq ;

namespace CodeMules.Commando {
	public partial class CommandoManager {
		#region Built-in Commands
		[CommandoCommand("cls",Alias="clear",Description="Clear the console")]
		private void ClearConsole() {
			Clear();
		}

		[CommandoCommand("echoargs",Alias="dumpargs",Description="Test argument processing")]
		private void EchoArgs(ICommandoLogWriter logWriter,params string [] args) {
			for(int i=0; i < args.Length; i++) {
				logWriter.WriteLine("args[{0}] = {1}",i,args[i]);
			}
		}

		[CommandoCommand(
			"help",
			Alias="?",
			Description="Display help for the console and registered commands"
		)]
		private void Help(ICommandoLogWriter logWriter,params string [] args) {
			if (args.Length == 0) {
				List<CommandoCommandAttribute> allCommands = Commands.Commands.ToList() ;
				allCommands.Sort((a,b) => string.Compare(a.Name,b.Name,true)) ;

				logWriter.WriteLine();

				foreach(CommandoCommandAttribute command in allCommands) {
					logWriter.WriteLine("{0,-16} {1}",command.Name,command.Description ?? "<no description>") ;
				}
			} else {
				IEnumerable<CommandoCommandAttribute> cmds = Commands.GetCommands(args[0]) ;
				
				int count = cmds.Count() ;
				
				if(count == 0) {
					logWriter.WriteLine(LogWriterStyle.Warning,"Command '{0}' not found",args[0]) ;
					return ;
				}
				
				if (count == 1) {
					logWriter.WriteLine("{0,-16} {1}",cmds.First().Name,cmds.First().Description ?? "<no description>") ;
				} else {
					List<CommandoCommandAttribute> allCommands = cmds.ToList() ;
					allCommands.Sort((a,b) => string.Compare(a.Name,b.Name,true)) ;
					
					logWriter.WriteLine();
					
					foreach(CommandoCommandAttribute command in allCommands) {
						logWriter.WriteLine("{0,-16} {1}",command.Name,command.Description ?? "<no description>") ;
					}
				}
			}
		}

		[CommandoCommand(
			"install",
			Description="Install commands from a Type"
		)]
		private void Install(ICommandoLogWriter logWriter,params string [] args) {
			if (args.Length > 0) {
				Type type = Type.GetType(args[0],false) ;

				if (type == null) {
					logWriter.WriteLine(
						LogWriterStyle.Error,
						"Unable to resolve type '{0}'. Did you include the namespace?",
						args[0]
					) ;

					return ;
				}

				int count = Commands.Commands.Count() ;

				Commands.AddCommandsForType
				(
					type,
					(attr,method,style,message) => {
						logWriter.WriteLine(style,message) ;
					}
				) ;

				int newCount = Commands.Commands.Count() ;

				if (newCount > count) {
					logWriter.WriteLine(
						LogWriterStyle.Success, 
						string.Format("Installed {0} new commands", newCount - count)
					);
				} else {
					logWriter.WriteLine(
						LogWriterStyle.Warning, 
						string.Format("No new commands were installed from {0}",args[0])
					);
				}
			} else {
				logWriter.WriteLine(LogWriterStyle.Warning,"The 'install' command requires 1 argument representing the type that contains the commands") ;
			}
		}
		#endregion
	}
}
