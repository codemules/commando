﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	The contents of this file are Proprietary and confidential. Unauthorized copying 
//	of this file, via any medium is strictly prohibited.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.SceneManagement ;
using UnityEditor ;

using System ;
using System.IO ;

namespace CodeMules.Commando.Unity {
	public static class UnityCommands {
		const string CommandNamespace = "unity" ;

		private static string GetYesNo(bool isYes) {
			return isYes ? "Yes" : "No" ;
		}

		[CommandoCommand(
			"sysinfo",
			Namespace=CommandNamespace,
			Description="Display information from UnityEngine.SystemInfo"
			)
		]
		private static void UnitySystemInfo(ICommandoLogWriter logWriter,params string [] args) {
			const int padLeft = 16 ;
			string seperator = new string('-',padLeft+1) ;
			string padFormat = string.Format("{{0,-{0}}}: {{1}}",padLeft);
			
			logWriter.WriteLine();
			logWriter.WriteLine("Device") ;
			logWriter.WriteLine(seperator) ;
			logWriter.WriteLine(padFormat,"Model",SystemInfo.deviceModel) ;
			logWriter.WriteLine(padFormat,"Name",SystemInfo.deviceName) ;
			logWriter.WriteLine(padFormat,"Type",SystemInfo.deviceType) ;
			logWriter.WriteLine(padFormat,"Id",SystemInfo.deviceUniqueIdentifier) ;
			logWriter.WriteLine(padFormat,"Accelerometer",GetYesNo(SystemInfo.supportsAccelerometer)) ;
			logWriter.WriteLine(padFormat,"Gyroscope",GetYesNo(SystemInfo.supportsGyroscope)) ;
			logWriter.WriteLine(padFormat,"Location",GetYesNo(SystemInfo.supportsLocationService)) ;
			logWriter.WriteLine(padFormat,"Vibration",GetYesNo(SystemInfo.supportsVibration)) ;
			logWriter.WriteLine(seperator) ;

			logWriter.WriteLine();
			logWriter.WriteLine("System") ;
			logWriter.WriteLine(seperator) ;
			logWriter.WriteLine(padFormat,"OS",SystemInfo.operatingSystem) ;
			logWriter.WriteLine(padFormat,"Memory (MB)",SystemInfo.systemMemorySize) ;
			logWriter.WriteLine(padFormat,"Processor",SystemInfo.processorType) ;
			logWriter.WriteLine(padFormat,"Proc. Count",SystemInfo.processorCount) ;
			logWriter.WriteLine(padFormat,"Proc. Freq (MHz)",SystemInfo.processorFrequency) ;
			logWriter.WriteLine(seperator) ;

			logWriter.WriteLine();
			logWriter.WriteLine("GPU") ;
			logWriter.WriteLine(seperator) ;
			logWriter.WriteLine(padFormat,"Name",SystemInfo.graphicsDeviceName) ;
			logWriter.WriteLine(padFormat,"Type",SystemInfo.graphicsDeviceType) ;
			logWriter.WriteLine(padFormat,"Id",SystemInfo.graphicsDeviceID) ;
			logWriter.WriteLine(padFormat,"Vendor",SystemInfo.graphicsDeviceVendor) ;
			logWriter.WriteLine(padFormat,"Vendor Id",SystemInfo.graphicsDeviceVendorID) ;
			logWriter.WriteLine(padFormat,"Version",SystemInfo.graphicsDeviceVersion) ;
			logWriter.WriteLine(seperator) ;
			
			logWriter.WriteLine();
			logWriter.WriteLine("GPU Specs") ;
			logWriter.WriteLine(seperator) ;
			logWriter.WriteLine(padFormat,"Memory",SystemInfo.graphicsMemorySize) ;
			logWriter.WriteLine(padFormat,"Multi-Threaded",GetYesNo(SystemInfo.graphicsMultiThreaded)) ;
			logWriter.WriteLine(padFormat,"Shader Level",SystemInfo.graphicsShaderLevel) ;
			logWriter.WriteLine(padFormat,"Max Texture",SystemInfo.maxTextureSize) ;
			logWriter.WriteLine(padFormat,"NPOT support",SystemInfo.npotSupport) ;
			logWriter.WriteLine(padFormat,"Render Targets",SystemInfo.supportedRenderTargetCount) ;
			logWriter.WriteLine(seperator) ;
			
			logWriter.WriteLine();
			logWriter.WriteLine("GPU Support") ;
			logWriter.WriteLine(seperator) ;
			logWriter.WriteLine(padFormat,"3D Textures",GetYesNo(SystemInfo.supports3DTextures)) ;
			logWriter.WriteLine(padFormat,"Image Effects",GetYesNo(SystemInfo.supportsImageEffects)) ;
			logWriter.WriteLine(padFormat,"Render Textures",GetYesNo(SystemInfo.supportsRenderTextures)) ;
			logWriter.WriteLine(padFormat,"Shadows",GetYesNo(SystemInfo.supportsShadows)) ;
			logWriter.WriteLine(padFormat,"Stencil",GetYesNo(Convert.ToBoolean(SystemInfo.supportsStencil))) ;
			logWriter.WriteLine(padFormat,"Sparse Textures",GetYesNo(SystemInfo.supportsSparseTextures)) ;
			logWriter.WriteLine(padFormat,"Render CubeMap",GetYesNo(SystemInfo.supportsRenderToCubemap)) ;
			logWriter.WriteLine(padFormat,"Instancing",GetYesNo(SystemInfo.supportsInstancing)) ;
			logWriter.WriteLine(padFormat,"Compute Shaders",GetYesNo(SystemInfo.supportsComputeShaders)) ;
			logWriter.WriteLine(padFormat,"Shadow Sampling",GetYesNo(SystemInfo.supportsRawShadowDepthSampling)) ;
			logWriter.WriteLine(seperator) ;		
		}	

		#if UNITY_EDITOR
		[CommandoCommand(
			"scenes",
			Namespace=CommandNamespace,
			Description="Display scenes in the Unity project",
			Syntax="scenes [-assets]"
		)
		]
		public static void ScenesCommand(ICommandoLogWriter logWriter,params string [] args) {
			bool useAssetDb = false ;

			if (args.Length > 0) {
				useAssetDb = (args[0] == "-assets") ;
			}

			if (useAssetDb) {
				foreach (string sceneGuid in AssetDatabase.FindAssets("t:Scene")) {
					string scenePath = AssetDatabase.GUIDToAssetPath(sceneGuid) ;
					string path = Path.GetDirectoryName(scenePath) ;
					string sceneName = Path.GetFileNameWithoutExtension(scenePath);			

					logWriter.WriteLine(string.Format("{0,-24} ./{1}",sceneName,path)) ;
				}

			} else {
				foreach(var scene in EditorBuildSettings.scenes) {
					logWriter.WriteLine(string.Format("{0,-48} Enabled: {1}",scene.path,scene.enabled)) ;
				}
			}
		}
		#endif

		[CommandoCommand(
			"loadscene",
			Namespace=CommandNamespace,
			Description="Load a unity scene",
			Syntax="loadscene {scene name}"
		)
		]
		public static void LoadSceneCommand(ICommandoLogWriter logWriter,params string [] args) {
			if (args.Length != 1) {
				logWriter.WriteLine(LogWriterStyle.Error,"Expecting the name of the scene to be specified");
				return ;
			}

			SceneManager.LoadScene(args[0]) ;
		}

		[CommandoCommand(
			"paths",
			Namespace=CommandNamespace,
			Description="Show Unity path values"
		)]
		public static void PathsCommand(ICommandoLogWriter logWriter,params string [] args) {
			const int padLeft = 17 ;
			string padFormat = string.Format("{{0,-{0}}}: {{1}}",padLeft);
			
			logWriter.WriteLine();
			logWriter.WriteLine(padFormat,"Data",Application.dataPath) ;
			logWriter.WriteLine(padFormat,"Persistant Data",Application.persistentDataPath) ;
			logWriter.WriteLine(padFormat,"Streaming Assets",Application.streamingAssetsPath) ;
			logWriter.WriteLine(padFormat,"Temp. Cache",Application.temporaryCachePath) ;
		}
		
		[CommandoCommand(
		"appinfo",
		Namespace=CommandNamespace,
		Description="Show information about the application"
		)]
		public static void AppInfoCommand(ICommandoLogWriter logWriter,params string [] args) {
			const int padLeft = 20 ;
			string padFormat = string.Format("{{0,-{0}}}: {{1}}",padLeft);
			
			logWriter.WriteLine();
			logWriter.WriteLine(padFormat,"Name",Application.productName) ;
			logWriter.WriteLine(padFormat,"Version",Application.version) ;
			logWriter.WriteLine(padFormat,"Bundle Id",Application.bundleIdentifier) ;
			logWriter.WriteLine(padFormat,"Company",Application.companyName) ;
			logWriter.WriteLine(padFormat,"Platform",Application.platform) ;
			logWriter.WriteLine(padFormat,"Run in Background",GetYesNo(Application.runInBackground)) ;
			logWriter.WriteLine(padFormat,"Target FPS",Application.targetFrameRate) ;
			logWriter.WriteLine(padFormat,"Install Mode",Application.installMode) ;
			
			if (Application.isWebPlayer) {
				logWriter.WriteLine(padFormat,"Url",Application.absoluteURL) ;
				logWriter.WriteLine(padFormat,"Security Enabled",GetYesNo(Application.webSecurityEnabled)) ;
				logWriter.WriteLine(padFormat,"Security Host Url",Application.webSecurityHostUrl) ;
			}
		}
	}
}
