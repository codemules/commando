﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	The contents of this file are Proprietary and confidential. Unauthorized copying 
//	of this file, via any medium is strictly prohibited.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using System ;
using System.Collections.Generic ;

namespace CodeMules.Commando {
	public interface ICommandoHistory {
		event EventHandler<ItemChangedEventArgs<string>> Changed ;

		void Add(string commandLine) ;
		string GetNextItem(ref int index) ;
		string GetPreviousItem(ref int index) ;
		int Count { get ; }
		IEnumerable<string> Items { get ; }
		string GetItem(int index) ;
		string this[int index] { get ; }
	}
}
