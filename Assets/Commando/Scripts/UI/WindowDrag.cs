﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	The contents of this file are Proprietary and confidential. Unauthorized copying 
//	of this file, via any medium is strictly prohibited.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.EventSystems;

namespace CodeMules.Commando.UI {
	public class WindowDrag : MonoBehaviour, IPointerDownHandler, IDragHandler {
		public RectTransform panelRectTransform;

		private Vector2 dragPoint;
		private Vector3 windowPosition;

		private RectTransform ParentRectTransform {
			get {
				return (panelRectTransform != null) ? panelRectTransform.parent as RectTransform : null ;
			}
		}

		public void OnPointerDown(PointerEventData eventData) {
			windowPosition = panelRectTransform.localPosition;

			RectTransformUtility.ScreenPointToLocalPointInRectangle(
				ParentRectTransform, 
				eventData.position, 
				eventData.pressEventCamera, 
				out dragPoint
				);
		}

		public void OnDrag(PointerEventData eventData) {
			if ((panelRectTransform == null) || (ParentRectTransform == null)) 
				return ;

			Vector2 pointerPosition;

			if (RectTransformUtility.ScreenPointToLocalPointInRectangle(
				ParentRectTransform, 
				eventData.position, 
				eventData.pressEventCamera, 
				out pointerPosition
				)) 
			{
				Vector3 offsetToOriginal = pointerPosition - dragPoint;
				panelRectTransform.localPosition = windowPosition + offsetToOriginal;
			}
		}
	}
}
