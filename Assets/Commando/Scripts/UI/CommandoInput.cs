﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	The contents of this file are Proprietary and confidential. Unauthorized copying 
//	of this file, via any medium is strictly prohibited.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI ;

namespace CodeMules.Commando.UI {
	public class CommandoInput : MonoBehaviour, ICommandoInput {
		private InputField inputField ;
		
		// Use this for initialization
		void Awake () {
			inputField = gameObject.GetComponent<InputField>() ;
		}
		
		public string Text { 
			get {
				return inputField.text ;
			}
			
			set {
				inputField.text = value ;
				inputField.MoveTextEnd(false) ;
			}
		}
		
		public bool IsFocused { 
			get {
				return inputField.isFocused ;
			}
		}
		
		public void Clear() {
			inputField.text = string.Empty ;
		}
		
		public void Activate() {
			inputField.Select() ;
			inputField.ActivateInputField() ;
			inputField.MoveTextEnd(false) ;
		}
	}
}
