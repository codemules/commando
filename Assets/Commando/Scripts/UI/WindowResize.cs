﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	The contents of this file are Proprietary and confidential. Unauthorized copying 
//	of this file, via any medium is strictly prohibited.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.EventSystems;

namespace CodeMules.Commando.UI {
	public class WindowResize : MonoBehaviour, IPointerDownHandler, IDragHandler {
		public RectTransform panelRectTransform;
		public Vector2 minimumSize = new Vector2(180, 160);

		private Vector2 dragPoint;
		private Vector2 sizeDelta;

		public void OnPointerDown(PointerEventData eventData) {
			sizeDelta = panelRectTransform.sizeDelta;

			RectTransformUtility.ScreenPointToLocalPointInRectangle(
				panelRectTransform, 
				eventData.position, 
				eventData.pressEventCamera, 
				out dragPoint
				);
		}

		public void OnDrag(PointerEventData data) {
			if (panelRectTransform == null)
				return;

			Vector2 position;

			RectTransformUtility.ScreenPointToLocalPointInRectangle(
				panelRectTransform, 
				data.position, 
				data.pressEventCamera, 
				out position
				);

			Vector3 offset = position - dragPoint;

			Vector2 delta = sizeDelta + new Vector2(offset.x, -offset.y);

			delta = new Vector2(
				Mathf.Clamp(delta.x, minimumSize.x, delta.x), 
				Mathf.Clamp(delta.y, minimumSize.y, delta.y)
			);

			panelRectTransform.sizeDelta = delta;
		}
	}
}
