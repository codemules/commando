﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	The contents of this file are Proprietary and confidential. Unauthorized copying 
//	of this file, via any medium is strictly prohibited.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI ;

namespace CodeMules.Commando.UI {
	public class CommandoConsoleLog : MonoBehaviour, ICommandoConsoleLog {
		Text consoleText ;
		
		// Use this for initialization
		void Start () {
			consoleText = gameObject.GetComponent<Text>();
		}
		
		public void UpdateConsole(string logText) {
			consoleText.text = logText ?? string.Empty ;
		}
	}
}
