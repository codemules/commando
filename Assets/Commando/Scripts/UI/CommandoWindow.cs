﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	The contents of this file are Proprietary and confidential. Unauthorized copying 
//	of this file, via any medium is strictly prohibited.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.UI ;

using System.Collections ;
using System.Collections.Generic ;
using System.Linq;

namespace CodeMules.Commando.UI {
	public class CommandoWindow : MonoBehaviour {
		private GameObject window ;
		private ICommandoInput input ;
		private ICommandoManager manager ;
		private ICommandoConsoleLog console ;
		private int historyIndex ;
		
		public Dropdown dropdown ;

		// Use this for initialization
		void Start () {
			Object.DontDestroyOnLoad(this) ;

			window = transform.FindChild("Window").gameObject ;
			input = window.GetComponentInChildren<ICommandoInput>() ;
			console = window.GetComponentInChildren<ICommandoConsoleLog>() ;
			
			Manager.ConsoleUpdated += UpdateConsoleLog ;
		}
		
		// Update is called once per frame
		void Update () {
			if (input == null)
				return ;
			
			bool toggleVisibility = false ;
			
			#if ((UNITY_IPHONE || UNITY_IOS) || UNITY_ANDROID) && !UNITY_EDITOR
		    if (Input.touchCount == 1) {
		      var touch = Input.GetTouch(0);
		      // Triple Tap shows/hides the console in iOS/Android dev builds.
			    toggleVisibility = (((touch.phase == TouchPhase.Canceled) || (touch.phase == TouchPhase.Ended)) && (touch.tapCount == 3)) ;
		    }
			#else
			toggleVisibility = Input.GetKeyDown(KeyCode.BackQuote) ;
			#endif
			
			// Show or hide the console window if the tilde key was pressed.
			if (toggleVisibility) {
				bool visible = !window.activeSelf;
				
				window.SetActive(visible);
				
				if (visible) {
					input.Activate();
				}
				
				input.Text = input.Text.TrimEnd('`') ;
			}
			
			#if UNITY_EDITOR || !(UNITY_IOS || UNITY_IPHONE || UNITY_ANDROID)
			bool isHistoryKey = false ;
			
			if (window.activeSelf && input.IsFocused) {
				if (Input.GetKeyDown(KeyCode.Return)) {
					ProcessCommand(input.Text);
				} else if (Input.GetKeyDown(KeyCode.UpArrow)) {
					isHistoryKey = true ;
					SetInput(Manager.History.GetPreviousItem(ref historyIndex)) ;
				} else if (Input.GetKeyDown(KeyCode.DownArrow)) {
					isHistoryKey = true ;
					SetInput(Manager.History.GetNextItem(ref historyIndex) ?? string.Empty) ;
				} else if (Input.GetKeyDown(KeyCode.Tab)) {
					// Autocomplete
					string partialInput = input.Text.Trim();
					if (partialInput != string.Empty) {
						SetInput(Manager.GetAutoCompletion(partialInput)) ;
					}
				}
				
				if (Input.anyKeyDown && !isHistoryKey) {
					historyIndex = Manager.History.Count ;
				}
			}
			#endif
		}

		private void SetInput(string text) {
			if (text != null) {
				input.Text = text ;
			}
		}
		
		private void UpdateConsoleLog(object sender,ConsoleUpdateEventArgs eventArgs) {
			console.UpdateConsole(eventArgs.LogText) ;
		}
		
		public void ProcessCommand() {
			ProcessCommand(input.Text) ;
		}
		
		private void ProcessCommand(string text) {
			text = text.Replace(System.Environment.NewLine,string.Empty).Trim() ;
			
			if (text.Length == 0) 
				return ;

			Manager.History.Add(text) ;
			
			Manager.ExecuteCommand(text) ;
			
			input.Clear() ;

			#if UNITY_EDITOR || !(UNITY_IOS || UNITY_IPHONE || UNITY_ANDROID)
			input.Activate() ;
			#endif
		}
		
		private ICommandoManager Manager {
			get {
				if (manager == null) {
					manager = new CommandoManager();
					
					manager.History.Changed += OnHistoryChanged ;
					manager.Commands.Changed += OnCommandsChanged ;
				}
				
				return manager ;
			}
		}
		
		private int commandSeparatorIndex ;
		
		public void OnCommandSelected(int index) {
			if (index == commandSeparatorIndex)
				return ;
			
			#if UNITY_EDITOR || !(UNITY_IOS || UNITY_IPHONE || UNITY_ANDROID)
			input.Activate() ;
			#endif
			
			StartCoroutine(SetCommandText(dropdown.options[index].text)) ;
		}
		
		private IEnumerator SetCommandText(string text) {
			yield return null ;
			
			SetInput(text) ;
		}
			
		private void RebuildCommandList() {
			commandSeparatorIndex = -1;

			List<string> commands =
				(from p in Manager.Commands.Commands select p.Name).ToList() ;
			
			if (Manager.History.Count > 0) {
				commandSeparatorIndex = commands.Count ;
				commands.Add("-- History --") ;
					
				commands.AddRange(Manager.History.Items) ;
			}
			
			dropdown.ClearOptions() ;
			dropdown.AddOptions(commands);
			dropdown.interactable = commands.Count > 1 ;
		}
		
		private void OnHistoryChanged(object sender,ItemChangedEventArgs<string> eventArgs) {
			RebuildCommandList();
		}

		private void OnCommandsChanged(object sender,ItemChangedEventArgs<CommandoCommandAttribute> eventArgs) {
			RebuildCommandList();
		}
	}
}
