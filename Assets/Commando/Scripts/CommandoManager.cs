﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	The contents of this file are Proprietary and confidential. Unauthorized copying 
//	of this file, via any medium is strictly prohibited.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using UnityEngine ;

using System ;
using System.Text ;
using System.Linq ;

namespace CodeMules.Commando {
	public partial class CommandoManager : ICommandoManager, ILogHandler {
		#region Fields
		StringBuilder consoleLog ;

		private ICommandoCommands commands ;
		private ICommandoHistory history ;

		private EventHandler<ConsoleUpdateEventArgs> consoleUpdateListeners ;
		#endregion

		#region Constructor(s)
		public CommandoManager() {
			consoleLog = new StringBuilder();

			Commands.AddCommands(this);
			Commands.AddCommandsForType(typeof(CodeMules.Commando.Unity.UnityCommands)) ;
		}
		#endregion

		#region ICommandoManager Implementation
		public string GetAutoCompletion(string text) {
			foreach(CommandoCommandAttribute attr in Commands.Commands) {
				if (attr.Name.StartsWith(text)) {
					return attr.Name ;
				} else if (!String.IsNullOrEmpty(attr.Alias) && attr.Alias.StartsWith(text)) {
					return attr.Alias ;
				}
			}
			
			return null ;
		}
		
		public event EventHandler<ConsoleUpdateEventArgs> ConsoleUpdated {
			add {
				consoleUpdateListeners += value ;
			}
			
			remove {
				consoleUpdateListeners -= value ;
			}
		}
		
		public void Clear() {
			consoleLog.Length = 0 ;
			OnUpdated();
		}
		
		public void ExecuteCommand(string commandLine) {
			indent = 0 ;

			LogWriter.WriteLine() ;
			LogWriter.WriteLine(">{0}",commandLine) ;


			string [] args = CommandoArguments.ParseCommandLine(commandLine).ToArray() ;
			string command = args[0] ;

			Func<ICommandoLogWriter,string [],object> commandFunc = Commands.GetCommandFunction(command) ;

			if (commandFunc != null) {
				ILogHandler previous = Debug.logger.logHandler ;
				Debug.logger.logHandler = this ;

				try {
					object result = commandFunc(this,args.Skip(1).ToArray());
					if (result != null) {
						LogWriter.WriteLine(result.ToString()) ;
					}
				} catch(Exception ex) {
					LogWriter.WriteLine(ex);
				} finally {
					Debug.logger.logHandler = previous ;
				}
			} else {
				LogWriter.WriteLine(LogWriterStyle.Error,"Command not found: '{0}'. Use 'help' or '?' for help",command) ;
			}
		}
		
		public ICommandoHistory History {
			get {
				if (history == null) {
					history = new CommandoHistory();
				}
				
				return history;
			}
		}

		public ICommandoCommands Commands {
			get {
				if (commands == null) {
					commands = new CommandoCommands();
				}

				return commands;
			}
		}

		public ICommandoLogWriter LogWriter { get { return this ; } }
		#endregion

		#region ICommandLogWriter Implementation
		private int indent = 0 ;
		private const string indentStr = "                               " ;

		public void NextIndent() {
			indent = Math.Min(8,indent+1) ;
		}

		public void PrevIndent() {
			indent = Math.Max(0,indent-1);
		}

		public string FormatWithStyle(LogWriterStyle style,string text) {
			switch(style) {
				case LogWriterStyle.Normal:
					break;
				case LogWriterStyle.Error:
					text = text.Color("#FF0000") ;
					break ;
				case LogWriterStyle.Success:
					text = text.Color("#2E8533FF") ;
					break ;
				case LogWriterStyle.Warning:
					text = text.Color("#D46A00FF");
					break ;
			}

			if (indent > 0) {
				text = indentStr.Substring(4*indent) + text ;
			}

			return text ;
		}

		public void WriteLine() {
			consoleLog.AppendLine();
			OnUpdated();
		}

		public void WriteLine(LogWriterStyle style,string logFormat,params object [] args) {
			consoleLog.AppendLine(FormatWithStyle(style,string.Format(logFormat,args))) ;
			OnUpdated() ;
		}

		public void WriteLine(LogWriterStyle style,string logText) {
			consoleLog.AppendLine(FormatWithStyle(style,logText)) ;
			OnUpdated() ;
		}

		public void WriteLine(string logFormat,params object [] args) {
			WriteLine(LogWriterStyle.Normal,logFormat,args);
		}
		
		public void WriteLine(string logText) {
			WriteLine(LogWriterStyle.Normal,logText);
		}

		public void WriteLine(Exception ex) {
			WriteLine(LogWriterStyle.Error,ex.ToString());
		}
		#endregion

		#region ILogHandler implementation

		public void LogFormat(LogType logType, UnityEngine.Object context, string format, params object[] args) { 
			LogWriterStyle style ;

			switch(logType) {
				case LogType.Error:
				case LogType.Assert:
					style = LogWriterStyle.Error ;
					break ;
				case LogType.Warning:
				case LogType.Exception:
					style = LogWriterStyle.Warning ;
					break ;
				default:
					style = LogWriterStyle.Normal ;
					break;
			}

			LogWriter.WriteLine(style,string.Format(format,args)) ;
		}

		public void LogException (Exception exception, UnityEngine.Object context) {
			LogWriter.WriteLine(exception);
		}

		#endregion

		#region Implementation
		private void OnUpdated() {
			if (consoleUpdateListeners != null) {
				consoleUpdateListeners(this,new ConsoleUpdateEventArgs(consoleLog.ToString())) ;
			}
		}
		#endregion
	}
}

