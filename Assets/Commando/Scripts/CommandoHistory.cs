﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	The contents of this file are Proprietary and confidential. Unauthorized copying 
//	of this file, via any medium is strictly prohibited.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using System ;
using System.Collections.Generic ;

namespace CodeMules.Commando {
	public class CommandoHistory : ICommandoHistory {
		private IDictionary<string,object> historyItems ;
		private EventHandler<ItemChangedEventArgs<string>> changedListeners ;

		private List<string> history ;

		public CommandoHistory() {
			historyItems = new Dictionary<string,object>();
			history = new List<string>();
		}

		public event EventHandler<ItemChangedEventArgs<string>> Changed {
			add {
				changedListeners += value ;

				if (history.Count > 0) {
					value(this,new ItemChangedEventArgs<string>(ItemChangeType.Add,string.Empty)) ;
				}
			}

			remove {
				changedListeners -= value ;
			}
		}

		public void Add(string commandLine) {
			if (!historyItems.ContainsKey(commandLine)) {
				historyItems.Add(commandLine,null) ;
				history.Add(commandLine);

				if (changedListeners != null) {
					changedListeners(this,new ItemChangedEventArgs<string>(ItemChangeType.Add,commandLine)) ;
				}
			}
		}

		public string GetNextItem(ref int currentIndex) {
			string result = null ;
			
			currentIndex = Math.Min(currentIndex+1,history.Count) ;
			
			if (currentIndex < history.Count) {
				result = history[currentIndex] ;
			}

			return result ;
		}
		
		public string GetPreviousItem(ref int currentIndex) {
			string result = null ;
			
			currentIndex = Math.Max(currentIndex-1,0) ;
			
			if (currentIndex >= 0) {
				result = history[currentIndex] ;
			}

			return result ;
		}
		
		public int Count { 
			get {
				return history.Count ;
			}
		}
		
		public IEnumerable<string> Items {
			get {
				return history ;
			}
		}
		
		public string GetItem(int index) {
			return history[index] ;
		}

		public string this[int index] {
			get {
				return GetItem(index);
			}
		}
	}
}
